# Contao 4 Captcha Bundle
Stellt entweder Google ReCaptcha v2 oder die hCaptcha Alternative für Formulare zur Verfügung.

## Konfiguration
Für beide Captcha-Anbieter muss bei Verwendung im jeweiligen Account der *SiteKey* sowie das *Secret* generiert werden.
Dieses wird dann in der Bundle-Konfiguration hinterlegt, in der `contao/config.yml`:
```yaml
rapid_data_captcha:
    sitekey: '612b2149-xxxx-4ff0-xxxx-907ef3050b9b'
    secret: '0xxxxxxxxx5a21b154E2b274041ff5aC72aAda591D2'
```
*__Tipp:__ Wenn die Konfiguration nicht vorhanden ist, wird automatisch versucht, `CAPTCHA_SITEKEY` und `CAPTCHA_SECRET` aus der `.env` Datei zu verwenden*

Soll **Google reCaptcha** verwendet werden, muss noch die Service-Konfiguration angepasst werden (der Standard ist hCaptcha):
```yaml
services:
    RapidData\CaptchaBundle\Service\CaptchaInterface:
        alias: 'RapidData\CaptchaBundle\Service\ReCaptchaService'
        public: true
```

Soll **Friendly Captcha** verwendet werden, muss noch die Service-Konfiguration angepasst werden (der Standard ist hCaptcha):
```yaml
services:
    RapidData\CaptchaBundle\Service\CaptchaInterface:
        alias: 'RapidData\CaptchaBundle\Service\FriendlyCaptchaService'
        public: true
```
