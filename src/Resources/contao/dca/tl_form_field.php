<?php
/**
 * Table tl_form_field
 */
$GLOBALS['TL_DCA']['tl_form_field']['palettes']['rapid_data_captcha'] = '{type_legend},type,label;{expert_legend:hide},class,tabindex,captcha_type,captcha_size,captcha_theme;';

$GLOBALS['TL_DCA']['tl_form_field']['fields']['captcha_type'] = [
    'inputType'     => 'select',
    'reference'     => &$GLOBALS['TL_LANG']['tl_form_field']['captcha_type'],
    'options' 		=> [
        'image',
        'audio'
    ],
    'sql'           => "varchar(10) NOT NULL default 'image'",
    'eval'          => [
        'tl_class' => 'w50'
    ],
];

$GLOBALS['TL_DCA']['tl_form_field']['fields']['captcha_size'] = [
    'inputType'     => 'select',
    'reference'     => &$GLOBALS['TL_LANG']['tl_form_field']['captcha_size'],
    'options' 		=> [
        'normal',
        'compact'
    ],
    'sql'           => "varchar(10) NOT NULL default 'normal'",
    'eval'          => [
        'tl_class' => 'w50'
    ],
];

$GLOBALS['TL_DCA']['tl_form_field']['fields']['captcha_theme'] = [
    'inputType'     => 'select',
    'reference'     => &$GLOBALS['TL_LANG']['tl_form_field']['captcha_theme'],
    'options' 		=> [
        'light',
        'dark'
    ],
    'sql'           => "varchar(10) NOT NULL default 'light'",
    'eval'          => [
        'tl_class' => 'w50'
    ],
];
