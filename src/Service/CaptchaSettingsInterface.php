<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Service;

interface CaptchaSettingsInterface
{
    /**
     * Returns the site key used to identify the site or application
     * the captcha is shown on.
     *
     * @return string The site key
     */
    public function getSiteKey(): string;

    /**
     * Returns the user secret to identify the owner of this application against the captcha API.
     *
     * @return string The secret
     */
    public function getSecret(): string;
}
