<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RapidDataCaptchaBundle extends Bundle
{
}
